var canvas = document.getElementById('canvas');
var ctx = canvas.getContext('2d');
var rectWidth = 150;
var rectHeight = 100;
var spacing = 10;
var isDown = false;
var startCoords = [];
var last = [0, 0];
var global = {
	scale	: 1,
	offset	: {
		x : 0,
		y : 0,
	},
};
var panning = {
	start : {
		x : null,
		y : null,
	},
	offset : {
		x : 0,
		y : 0,
	},
};
canvas.width	= 800;
canvas.height	= 600;
ctx.fillStyle = 'rgba(0,0,0,1)';

var drawRect = function(x, y, rgba) {
    ctx.fillRect(x, y, rectWidth, rectHeight);
}

canvas.addEventListener("mousedown", startPan);

function pan() {
    if(panning.start.x) {
        requestAnimationFrame(pan);
    }

    // ctx.save();
	ctx.setTransform(1, 0, 0, 1, 0, 0);
	ctx.clearRect(0, 0, canvas.width, canvas.height);
	ctx.translate(panning.offset.x, panning.offset.y);
    // ctx.restore();
	draw();
}

function startPan(e) {
	window.addEventListener("mousemove", trackMouse);
	window.addEventListener("mousemove", pan);
	window.addEventListener("mouseup", endPan);
	panning.start.x = e.clientX;
	panning.start.y = e.clientY;

    requestAnimationFrame(pan);
}

function endPan(e) {
	window.removeEventListener("mousemove", trackMouse);
	window.removeEventListener("mousemove", pan);
	window.removeEventListener("mouseup", endPan);
	panning.start.x = null;
	panning.start.y = null;
	global.offset.x = panning.offset.x;
	global.offset.y = panning.offset.y;
}

function trackMouse(e) {
	var offsetX	 = e.clientX - panning.start.x;
	var offsetY	 = e.clientY - panning.start.y;
	panning.offset.x = global.offset.x + offsetX;
	panning.offset.y = global.offset.y + offsetY;
}

var draw = function() {
    var max = 100;
    var x = 10;
    var y = 10;
    for(var i = 0; i < max; i++) { //Horizontal

        y = 10;

        for(var j = 0; j < max; j++) { //Vertical
            drawRect(x, y);
            y += rectHeight;
            y += spacing;
        }
        x += rectWidth;
        x += spacing;
    }
}
draw();
